class AccountActivationsController < ApplicationController

  def edit
    user = User.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(:activation, params[:id])
      user.activate
      log_in user
      flash[:success] = "حساب کاربری فعال شد"
      redirect_to user
    else
      flash[:danger] = "پیوند فعال سازی نامعتبر است"
      redirect_to root_url
    end
  end
end
