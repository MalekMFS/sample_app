class SessionsController < ApplicationController

  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_back_or user
      else
        message  = "حساب کاربری فعال نشده "
        message += "برای فعال سازی پست الکترونیک خود را بررسی کنید"
        flash[:warning] = message
        redirect_to root_url
      end
    else
      flash.now[:danger] = 'نام کاربری یا رمز عبور صحیح نمی باشد'
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end